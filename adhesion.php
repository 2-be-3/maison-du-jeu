<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Maison du Jeu</title>
</head>
<body>
<div class="wrapper">
    <?php include "partials/header.php";?>
    <div class="container mt-4 mb-4">
        <h1>Adhérer</h1>
        <div class="row">
            <div class="col-sm-8">
                <form class="row g-3">
                    <div class="col-md-6">
                        <label for="name" class="form-label">Nom</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="col-md-6">
                        <label for="surname" class="form-label">Prénom</label>
                        <input type="text" class="form-control" id="surname" name="surname">
                    </div>
                    <div class="col-md-6">
                        <label for="naissance" class="form-label">Date de naissance</label>
                        <input type="date" class="form-control" id="naissance">
                    </div>
                    <div class="col-md-6">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" name="email">
                    </div>
                    <div class="col-md-6">
                        <label for="password" class="form-label">Mot de passe</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <div class="col-md-6">
                        <label for="confirm" class="form-label">Confirmation</label>
                        <input type="password" class="form-control" id="confirm" name="confirm">
                    </div>
                    <div class="col-md-12">
                        <label for="type-adhesion" class="form-label">Adhésion</label>
                        <select name="type-adhesion" class="form-select" id="type-adhesion">
                            <option value="individuelle">Adhésion Individuelle</option>
                            <option value="familiale">Adhésion Familiale</option>
                            <option value="structure">Adhésion Structure</option>
                            <option value="assistant-maternel">Adhésion Assistant-e Maternel-le</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <input class="form-check-input" type="checkbox" id="condition" name="condition"/>
                        <label for="condition" class="form-label"> J'accepte les <a href="#">Conditions d'utilisation</a></label>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary">Adhérer</button>
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="card-title">Adhésion Individuelle</h5>
                        <ul class="card-text">
                            <li>20€/an</li>
                            <li>Tarif réduit : 10€/an (étudiant-e, sans emploi, retraité-é...)</li>
                        </ul>
                    </div>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="card-title">Adhésion Assistant-e Maternel-le</h5>
                        <div class="card-text">
                            <ul>
                                <li>20€/an</li>
                            </ul>
                            <p>
                                Cette adhésion permet à un-e Assistant-e Maternel-le de venir gratuitement, accompagné de 5 enfants maximum.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="card-title">Adhésion Structure</h5>
                        <ul class="card-text">
                            <li>35€/an</li>
                        </ul>
                    </div>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="card-title">Adhésion individuelle</h5>
                        <div class="card-text">
                            <ul>
                                <li>40€/an si il y a 2 salaires au sein du foyer</li>
                                <li>30€/an si il y a 1 salaire au sein du foyer</li>
                                <li>20€/an si il n'y a aucun salaire au sein du foyer</li>
                            </ul>
                            <p>
                                Une adhésion Familiale prend en compte 2 adultes et les enfants du foyer
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "partials/footer.php";?>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>

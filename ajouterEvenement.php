
<!doctype html>
<?php

require "./calendrier/calendrier.php";
require "functionBDD/functions.php";

$couleur = array("Fermetures / Congés"=>"bg-danger","Portes ouvertes"=>"bg-info","Spectacle"=>"bg-warning","Initiation aux jeux"=>"bg-success");



?>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Ajouter un évenement</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">

    <?php include "./partials/header.php" ?>

</head>
<body>
    <div class="container mt-4 mb-4">
        <h1>Ajouter un évènement</h1>
        <form class="row">
            <div class="col-12">
                <label for="evenement" class="form-label">Intitulé de l'évenement:</label>
                <input type="text" id="evenement" name="evenement" required class="form-control">
            </div>
            <div class="col-6">
                <label for="date" class="form-label">Jour de l'évenement:</label>
                <input type="date" id="date" name="date" required class="form-control">
            </div>
            <div class="col-6">
                <label for="couleur" class="form-label">Type d'évènement</label>
                <select name="couleur" id="couleur" class="form-select">
                    <?php foreach (getLesCouleurs() as $coul){
                        echo "<option value=".$coul['id'].">".$coul['nom']."</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-12">
                <label for="description" class="form-label">Description :</label>
                <textarea type="text" id="description" name="description" class="form-control" required></textarea>
            </div>
            <div class="col-12 mt-4">
                <button class="btn btn-primary" type="submit">Valider</button>
            </div>
        </form>

        <?php if (isset($_GET['date']) && isset($_GET['evenement'])&& isset($_GET['description'])&& isset($_GET['couleur'])) {
            ajouteEventBDD($_GET['date'], $_GET['evenement'], $_GET['description'],$_GET['couleur']);
            if(!isset($_SESSION['listeevent'])){
                $_SESSION['listeevent'] = [];
            }
        }?>


        <div class="my-4">
            <a href="./calendrier.php">Retour</a>
        </div>
    </div>
    <?php include "./partials/footer.php" ?>
</body>
</html>


<!doctype html>
<?php
require "functionBDD/functions.php";

creerEvent();
require "./calendrier/calendrier.php";


$annee = 2021;

$mois = array(1 => "Janvier",2 => "Fevrier",3 => "Mars",4 => "Avril",5 => "Mai",6 => "Juin",7 => "Juillet",8 => "Aout",
    9 => "Septembre",10 => "Octobre",11 => "Novembre",12 => "Decembre");

?>

<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Evenement</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">


</head>
<body>
<?php include "./partials/header.php"; ?>
<div class="container mt-4">
    <form class="row g-3">
        <div class="col-md-2">
            <label for="LeMois" class="form-label">Mois à afficher :</label>
            <select name="mois" id="LeMois" class="form-select">
                <?php
                foreach ($mois as $unMois)
                    echo '<option value="'.$unMois.'">'.$unMois.'</option>';
                ?>
            </select>
        </div>
        <div class="col-md-4">
            <label for="annees" class="form-label">Année comprise entre 2020 et 2030:</label>
            <input type="number" id="annees" name="annees"
                   min="2020" max="2030" value="<?= $annee ?>" required class="form-control" />
        </div>
        <div class="col-12">
            <button class="btn btn-primary" type="submit">Valider</button>
        </div>
    </form>
</div>
<div class="container">
    <?php
    if(isset($_GET['mois'])&&isset($_GET['annees'])) {
        $month = new Calendrier(array_search($_GET['mois'], $mois), $_GET['annees']);
    }else{
        $month = new Calendrier(array_search('Janvier', $mois), 2021);
    }
        $start = $month->getStartingDay();
        $start = $start -> format('N') === '1' ? $start : $month->getStartingDay()->modify("last monday");
        ?>

        <h1><?= $month->toString(); ?></h1>

</div>
<div class="container">
    <table class="table table-bordered">
        <?php for ($i = 0; $i < $month->getWeeks();$i++){ ?>
        <tr>
            <?php foreach ($month -> days as $k => $day){
                $date = (clone $start)->modify("+" . ($k + $i*7) . "days");
                $matchedEvent = [];
                $ann = 2021;
                if(isset($_GET["annees"])){
                    $ann = $_GET["annees"];
                }
                $events = [];
                if(isset($_SESSION['listeevent'])){
                    $events =$_SESSION['listeevent'];
                }
                foreach ($events as $event){
                    if($ann == $event->annee and $date->format("m") == $event->mois and $date->format("d") == $event->jour ){
                        $matchedEvent[] = $event;
                    }
                }

                if(sizeof($matchedEvent) != 0){
                    $idEvent = 0;

                    for ($y = 0; $y < sizeof($events); $y++){

                        if($events[$y] == $matchedEvent[0])
                            $idEvent = $y;
                    }
                    ?>
                    <td class="col-md-1 <?= $month->withinMonth($date) ? : 'calendar_othermonth' ?>" style="background-color: <?= getUneCouleur($matchedEvent[0]->id)[0]["couleur"] ?>;">
                        <div><strong><?= $day;?></strong></div>
                        <div><?= $date ->format('d'); ?></div>
                        <!--<div><a href='PageEvenement.php?date=<?/*=$matchedEvent[0]->annee*/?>-<?/*=$matchedEvent[0]->mois*/?>-<?/*=$matchedEvent[0]->jour*/?>&evenement=<?/*=$matchedEvent[0]->libelle*/?>&couleur=<?/*=$matchedEvent[0]->couleur*/?>&description=<?/*=$matchedEvent[0]->description*/?>' ><?/*= $matchedEvent[0]->libelle */?></a></div>-->
                        <div><a href="evenement.php?id=<?= $idEvent ?>"><?= $matchedEvent[0]->libelle ?></a></div>


                    </td>
                    <?php
                }else {
                    ?>
                    <td class="col-md-1 <?= $month->withinMonth($date) ? '' : 'calendar_othermonth' ?>">
                        <div><strong><?= $day;?></strong></div>
                        <div><?= $date ->format('d'); ?></div>
                        <div></div>
                    </td>
                    <?php
                }
                ?>
            <?php }
            } ?>
    </table>

</div>
<div class="container">
    <h1>Légendes :</h1>
    <?php
    foreach (getLesCouleurs() as $uneCouleur){
        echo '<svg width="20" height="20" style="background-color: '.$uneCouleur['couleur'].'" ></svg> : '.$uneCouleur['legende'] .'<br/>';

    }
    ?>

</div>
<br>
<div class="container">
    <a href="ajouterEvenement.php">Ajouter un évènement</a>

</div></br>
<?php include "partials/footer.php" ?>
</body>
</html>

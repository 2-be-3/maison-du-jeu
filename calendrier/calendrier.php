<?php



Class Calendrier{




    public array $days = array("Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche");
    private array $months = array("Janvier","Fevrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Decembre");


    private ?int $month;
    private ?int $year;

    /**
     * Month Constructor
     * @param int|null $month
     * @param int|null $year
     * @throws Exception
     */

    public function __construct(?int $month = null, ?int $year =  null)
    {
        if ($month === null){
            $month = intval(date("m"));
        }
        if ($year === null){
            $year = intval(date("Y"));
        }

        if ($month <1 || $month >12) {
            throw new Exception("Le mois $month n'est pas valide");
        }
        if ($year<2000) {
            throw new Exception("L'année est incorrecte");
        }
        $this->month = $month;
        $this->year = $year;



    }


    /**Renvoie le premier Jour du mois
     * @return DateTime
     */
    public function getStartingDay(): DateTime
    {
    return new DateTime("{$this->year}-{$this->month}-01");
    }

    /**retourner le mois indiquer
     * @return string
     */

    public function toString() :string {
        return $this->months[$this->month-1]. ' '.$this->year;
    }

    public function getWeeks(): int
    {
        $start = $this->getStartingDay();
        $end = (clone $start)->modify('+1 month -1 day');
        $weeks =  intval($end->format('W')) - intval($start->format('W')) + 1;
        if ($weeks < 0){
            $weeks = intval($end->format('W')) + 1;
        }
        return $weeks;
    }

    public function withinMonth(DateTime $date):bool{
        return $this->getStartingDay()->format('Y-m') === $date->format('Y-m');

    }

}
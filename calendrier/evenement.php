<?php
Class Evenement{

    public int $id;
    public int $jour;
    public int $mois;
    public int $annee;
    public string $libelle;
    public string $description;
    public string $couleur;

    function __construct($id,$date,$intitule,$descrip,$coul)
    {
        $dateInDateFormat = getdate(strtotime($date));
        $this->id = $id;
        $this->jour = $dateInDateFormat["mday"];
        $this->mois = $dateInDateFormat["mon"];
        $this->annee = $dateInDateFormat["year"];
        $this->libelle = $intitule;
        $this->description = $descrip;
        $this->couleur = $coul;
    }

    public function getLibelle() : string{
        return $this->libelle;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getDate() : string
    {
        return "{$this->annee}-{$this->mois}-{$this->jour}";
    }

    public function getCouleur():string
    {
        return "$this->couleur";
    }

    public function getId():string{
        return  "$this->id";
    }
}
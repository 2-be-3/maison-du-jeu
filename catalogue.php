<!DOCTYPE html>
<?php
 include_once 'functionBDD/functions-catalogue.php';
 $games = getGames();
?>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Maison du Jeu</title>
</head>
<body>
<div class="wrapper">
    <?php include "partials/header.php";?>
    <div class="container mt-4">
        <h1>Catalogue</h1>
        <div class="row row-cols-1 row-cols-lg-4 g-2 g-lg-3">
            <?php
            foreach ($games as $game){
                $gameCategories = getGameCategories($game['idCategorieCatalogue']);
                ?>
                <div class="col">

                    <div class="border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                        <img src="./assets/img/<?= $game['image'] ?>" class="img-fluid"  alt="7 Wonders"/>
                        <div class="col p-4 d-flex flex-column position-static">
                            <small class="d-inline-block mb-2 text-info font-weight-bold">
                                <?= $gameCategories ?>
                            </small>
                            <h5 class="mb-0"><?= $game['nom'] ?> </h5>
                            <p class="text-justify mb-auto"> <?= $game['description'] ?></p>
                            <strong class="mb-auto"><?= $game['point'] ?> points</strong>
                            <a href="jeu.php?id=<?= $game['id'] ?>" class="btn btn-info">Voir le jeu</a>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <?php include "partials/footer.php";?>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>

<?php
require "functionBDD/functions.php";
require "./calendrier/calendrier.php";

if(!isset($_GET['id']) or $_SESSION['listeevent'][$_GET['id']] == null)
    header('Location: ./calendrier.php');
$event = $_SESSION['listeevent'][$_GET['id']];
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Évènement</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<?php
    include "./partials/header.php";
    ?>

   <div class="container">

    <br/><h1>Évènement</h1>
    Nom : <?=$event->getLibelle()?> <br/>
    Date : <?=$event->getDate()?> <br/>
    Description : <?=$event->getDescription()?><br/>
    <svg width="20" height="20" style="background-color: <?= getUneCouleur($event->getId())[0]["couleur"] ?>;"></svg> : <?= getUneCouleur($event->getId())[0]["legende"] ?>
    </div><br/>

<div class="container">
    <a href="calendrier.php">Retour</a>

</div><br />
<?php include "./partials/footer.php" ?>
</body>
</html>
<?php
function connexion()
{
    $connexion = pg_connect("host=192.168.222.86 port=5432 dbname=maisondujeuTEST user=the2be3 password=the2be3");
    if (!$connexion) {
        print("erreur conexion");
    } else {
        return $connexion;
    }

}

function getJours($categorie)
{
    $result = pg_query(connexion(),"SELECT jour,horaire.id FROM horaire inner join horairecategorie on horaire.idcategorie = horairecategorie.id where categorie ='".$categorie."';");
    $listeJours = pg_fetch_all($result);
    return $listeJours;
}

function getHorairesByJour($idJour)
{
    $result = pg_query(connexion(), 'SELECT * FROM "horairePeriode" inner join "horaire_horairePeriode" hhP on "horairePeriode".id = hhP."idHorairePeriode" where "idHoraire"='.$idJour.';');
    $listeHoraireByJour = pg_fetch_all($result);
    return $listeHoraireByJour;
}


function dataTarif(){
    $conn = connexion();
    $result = pg_query($conn, 'Select * from "grilleTarifJeu" inner join JoursUtilisations on "grilleTarifJeu".idjourutilisation = JoursUtilisations.id');
    $infos = pg_fetch_all($result);
    return $infos;
}

function jourutilisation($lespointscumules){
    $conn = connexion();
    pg_prepare($conn,'lesDonnees','Select * from "grilleTarifJeu" where nbpointcumule = $1 order by id');
    $result = pg_execute($conn,'lesDonnees',[$lespointscumules]);
    $infos = pg_fetch_all($result);
    return $infos;
}

?>
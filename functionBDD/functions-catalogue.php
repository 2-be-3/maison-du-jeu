<?php

function getConnection(){
    return pg_connect("host=192.168.222.86 dbname=maisondujeuTEST user=the2be3 password=the2be3 port=5432");
}

function getGames(){
    pg_prepare(getConnection(), "getGames", "SELECT * FROM jeu;");
    $result = pg_execute(getConnection(), "getGames", []);
    return pg_fetch_all($result);
}

function getGameCategories($id){
    pg_prepare(getConnection(), "getGameCategories", 'SELECT cJC.nom FROM "categorieJeuCatalogue" cJC where cJC.id=$1;');
    $result = pg_execute(getConnection(), "getGameCategories", [$id]);
    $categories = "";
    foreach(pg_fetch_all($result) as $cat){
        $categories = $cat['nom'];
    }
    return $categories;
}

function getGame($id): array
{
    pg_prepare(getConnection(), "getGame", 'SELECT * from jeu where jeu.id=$1');
    $result = pg_execute(getConnection(), "getGame", [$id]);
    return pg_fetch_all($result)[0];
}
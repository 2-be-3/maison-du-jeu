<?php
require "calendrier/evenement.php";
session_start();




function getConnexion(){
    return pg_connect("host=192.168.222.86 port=5432 dbname=maisondujeuTEST user=the2be3 password=the2be3");
}

function getLesEvenements(){
    $lesEvenements = pg_query(getConnexion(),'Select evenement.id,evenement.nom,couleur,description,legende,date from evenement inner join "categorieEvenement" cE on cE.id = evenement."idCategorieEvenement";');
    return pg_fetch_all($lesEvenements);
}

function creerEvent(){
    if(!isset($_SESSION['listeevent'])) $_SESSION['listeevent'] = [];
    foreach (getLesEvenements() as $event){
        array_push($_SESSION['listeevent'],new Evenement($event['id'],$event['date'],$event['nom'],$event['description'],$event['couleur']));
    }
}

function getLesCouleurs(){
    $lesCouleurs = pg_query(getConnexion(),'Select id,nom,couleur,legende from "categorieEvenement"');
    return pg_fetch_all($lesCouleurs);
}

function getUneCouleur($idCouleur){
    pg_prepare(getConnexion(),"uneCouleur",'Select id,nom,couleur,legende from "categorieEvenement" where id=$1');
    $uneCouleur = pg_execute(getConnexion(),"uneCouleur",[$idCouleur]);
    return pg_fetch_all($uneCouleur);
}

function ajouteEventBDD($idCategorieEvenement,$nom,$description,$date){
    pg_prepare(getConnexion(),"ajouteEvent",'insert into evenement ("idCategorieEvenement",nom,description,date) Values ($1,$2,$3,$4)');
    pg_execute(getConnexion(),"ajouteEvent",[$date,$nom,$description,$idCategorieEvenement]);
    header("Location: ./calendrier.php");
}
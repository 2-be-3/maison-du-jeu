<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Maison du Jeu</title>
</head>
<body>

<?php
    include "partials/header.php";
    include 'functionBDD/functions.php';

?>

<main>

    <div id="myCarousel" class="carousel slide" data-bs-ride="carousel">
        <ol class="carousel-indicators">
            <li data-bs-target="#myCarousel" data-bs-slide-to="0" class="active"></li>
            <li data-bs-target="#myCarousel" data-bs-slide-to="1"></li>
            <li data-bs-target="#myCarousel" data-bs-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="./assets/img/solitaire.jpg" class="bd-placeholder-img" />


                <div class="container">
                    <div class="carousel-caption text-start">
                        <h1>Tournoi de solitaire</h1>
                        <p>Venez participer à notre grand tournoi régional de solitaire, le 21 février</p>
                        <p><a class="btn btn-lg btn-primary" href="#" role="button">Plus d'informations</a></p>
                    </div>
                </div>s
            </div>
            <div class="carousel-item">
                <img src="./assets/img/mdj.jpg" class="bd-placeholder-img" />
                <div class="container">
                    <div class="carousel-caption text-start">
                        <h1>Porte ouverte</h1>
                        <p>Grande porte ouverte le 8 mars. Venez essayez nos jeux et rencontrez notre équipe !</p>
                        <p><a class="btn btn-lg btn-primary" href="#" role="button">Plus d'informations</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">

                <img src="./assets/img/escape-games.jpg" class="bd-placeholder-img" />
                <div class="container">
                    <div class="carousel-caption text-start">
                        <h1>Initiation</h1>
                        <p>Venez participez à notre escape games, organisé par une structure locale</p>
                        <p><a class="btn btn-lg btn-primary" href="#" role="button">Plus d'informations</a></p>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </a>
    </div>


    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Catalogue</h2>
                <p class="lead">Entrainez vous avant notre prochain évènement ! Vous trouverez toujours un jeu qui correspondra à l'âge de votre enfant ou à ce que vous voulez jouer ! venez voir notre catalogue.</p>
                <p><a class="btn btn-lg btn-primary" href="./catalogue.php" role="button">Notre catalogue</a></p>
            </div>
            <div class="col-md-5">
                <img src="https://psycatgames.com/fr/magazine/party-games/yahtzee/feature-image_hua83eb0149b1b8e0f90c330a5ee4b914f_621836_1200x1200_fill_q75_box_center.jpg" alt="" class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto">

            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7 order-md-2">
                <h2 class="featurette-heading">Informations pratiques</h2>
                <p class="lead">Pour toutes demandes vous retrouverez sur cette page notre contact ainsi que notre plan d'accès. Vous y retrouverez également nos horaires d'ouverture.</p>
                <p><a class="btn btn-lg btn-primary" href="./infos.php" role="button">Informations d'ouverture</a></p>
            </div>
            <div class="col-md-5 order-md-1">
                <img src="./assets/img/infos.png" alt="" class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto">

            </div>
        </div>

        <hr class="featurette-divider">

        <!-- /END THE FEATURETTES -->

    </div><!-- /.container -->


    <!-- FOOTER -->
    <?php include "partials/footer.php" ?>
</main>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>

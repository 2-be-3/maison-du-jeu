<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Maison du Jeu</title>
</head>
<body>


<?php
include "partials/header.php";
include "./functionBDD/fonction_Info_Tarifs.php";
$listeJourSalleDeJeux = getJours("Salle de jeux");
$listeJourServiceDePret = getJours("Service de prêt");
?>

<main>

    <!--Horaire-->
    <div class="container mt-3">
        <div class="row row-cols-1 row-cols-lg-2 g-2 g-lg-3">
            <div class="col">
                <div class=" overflow-hidden flex-md-row mb-4 h-md-250 position-relative">
                    <div class="col p-4 d-flex flex-column position-static">

                        <h5 class="mb-0">Horaire de la salle de jeux</h5><br/>
                        <p class="text-justify mb-auto">

                            <?php
                            foreach ($listeJourSalleDeJeux as $jour){?>
                                <strong> <?= $jour['jour'] ?>:</strong><br/>
                                <?php
                                $horaires = getHorairesByJour($jour['id']);
                                if($horaires){
                                    foreach ($horaires as $horaire){?>
                                        <?= $horaire['debut'] ?> - <?= $horaire['fin'] ?> : <?= $horaire['description'] ?><br/>
                                        <?php
                                    }
                                }else {

                                    print("Fermé<br/>");
                                }
                                ?>
                            <?php }?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="overflow-hidden flex-md-row mb-4 h-md-250 position-relative">
                    <div class="col p-4 d-flex flex-column position-static">

                        <h5 class="mb-0">Horaire du service de prêt</h5><br/>
                        <?php
                        foreach ($listeJourServiceDePret as $jour){?>
                            <strong> <?= $jour['jour'] ?>:</strong>
                            <?php
                            $horaires = getHorairesByJour($jour['id']);
                            if($horaires){
                                foreach ($horaires as $horaire){?>
                                    <?= $horaire['debut'] ?> - <?= $horaire['fin'] ?> : <?= $horaire['description'] ?><br/>
                                    <?php
                                }
                            }else {

                                print("Fermé<br/>");
                            }
                            ?>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!--Infos-->
    <div class="container">
        <div class="row row-cols-1 row-cols-lg-2 g-2 g-lg-3">
            <div class="col">
                <div class=" overflow-hidden flex-md-row mb-4 h-md-250 position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <p class="text-justify mb-auto">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            +46 850 96 753 2<br/>
                        </p>
                        <p class="text-justify mb-auto">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            maisondujeu@chepaou.com<br/>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class=" overflow-hidden flex-md-row mb-4 h-md-250 position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <p class="text-justify mb-auto">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            7 rue Lost<br/>
                            87460 Random - Île Chepaou
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Google map-->
    <div id="map-container-google-1" class="z-depth-1-half map-container" style="height: 500px">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10031659.34408585!2d-72.61129199749!3d26.677531154243905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89451ab5034cb7ab%3A0xb600ecf3df7aca4d!2sTriangle%20des%20Bermudes!5e0!3m2!1sfr!2sfr!4v1612349635071!5m2!1sfr!2sfr" frameborder="0"
                style="border:0" allowfullscreen></iframe>
    </div>


    <!-- FOOTER -->
    <?php include "partials/footer.php" ?>
</main>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>
<?php
require_once 'functionBDD/functions-catalogue.php';
session_start();
if(!isset($_GET['id']) or getGame($_GET['id']) == [])
    header('Location: ./catalogue.php');
$game = getGame($_GET['id']);

?>
<!doctype html>

<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Jeu</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<?php
include "./partials/header.php";
?>
<div class="container mt-5">
    <div class="row row-cols-1 row-cols-lg-2 d-flex justify-content-center">
        <div class="card col">
            <div class="card-body">
                <h5 class="card-title"><?= $game['nom'] ?> - <?= $game['point'] ?> points</h5>
                <p class="card-subtitle text-muted"><?= $game['age'] ?> ans et + <br/> jusqu'à <?= $game['nbjoueur'] ?> joueurs <br/> Environ <?= $game['duree'] ?> minutes</p>
                <br />
                <p class="card-text"><?= $game['description'] ?></p>
            </div>
            <img src="./assets/img/<?= $game['image'] ?>" class="card-img-bottom" alt="...">
        </div>
    </div>

</div>
<?php include "./partials/footer.php" ?>
</body>
</html>
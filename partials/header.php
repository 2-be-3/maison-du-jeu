<header>
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="/">MDJ</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="./index.php">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./catalogue.php">Catalogue</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./tarif.php">Tarif</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./calendrier.php">Agenda</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./adhesion.php">Adhérer</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./infos.php">Informations Pratiques</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./testpage.php">Test fonction Cal</a>
                    </li>
                </ul>
                <form class="d-flex">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>
</header>

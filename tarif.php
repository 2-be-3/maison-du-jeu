<!DOCTYPE html>
<?php
require 'functionBDD/fonction_Info_Tarifs.php';

function afficherGrille(array $lestarifs)
{
    if (sizeof($lestarifs) != 0) {

        print("<table class='table table-bordered table-striped container'><thead class='table-dark '> 
                <tr><th rowspan='2' class='col-md-1'>nombre de points cumulés par jeux empruntés</th><th colspan='7' class='text-center'>Nombre de jour d'emprunt</tr>
                <tr class='text-center'><th class='col-md-1'>1 jour</th> <th class='col-md-1'>2 jours</th> <th class='col-md-1'>3 jours</th> <th class='col-md-1'>4 jours</th> <th class='col-md-1'>5 jours</th> <th class='col-md-1'>6 jours</th>  <th class='col-md-1'>Week-end</th> </tr>
                </thead><tbody>");
        for($j = 1; $j <= 19 ; $j ++) {
            print("<tr><td>".$j."</td>");
            foreach (jourutilisation($j) as $tarif){
                if($tarif['prix'] == 15){
                    print("<td class='bg-info'>" . $tarif['prix']  . " €". "</td>");
                }else {
                    print("<td>" . $tarif['prix'] . " €". "</td>");
                }
            }
            print("</tr>");
        }
        print("</tbody></table>");

    } else {
        print("Aucun tarif<br>");
    }
}

?>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Maison du Jeu</title>
</head>
<body>
<div class="wrapper">
    <br/>
    <div class="container">
        <h1>Tarif</h1>
    </div>
    <br/>
    <?php include "partials/header.php";

        afficherGrille(dataTarif());
        print('<br/>');
    ?>
    <div class="container">
        <h1> Légende :</h1>
        <p> <svg width="20" height="20" class="bg-info"></svg>  <span> Prix constant</span></p>
    </div>
    <br/>
    <?php include "partials/footer.php";?>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>
